<!DOCTYPE html>
<html>
<head>
    <title>Contact Manager</title>
    <meta name="viewport" content="initial-scale=1" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">
    <link rel="stylesheet" href="webjars/angular-material/1.0.7/angular-material.css">
    <link rel="stylesheet" href="webjars/angular-material-icons/0.7.0/angular-material-icons.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <style>
        #main {
            height: 100%;
            max-height: 100%;
        }
    </style>
    <script src='webjars/angularjs/1.5.5/angular.js'></script>
    <script src='webjars/angular-ui-router/0.2.18/angular-ui-router.js'></script>
    <script src='webjars/angularjs/1.5.5/angular-aria.js'></script>
    <script src='webjars/angularjs/1.5.5/angular-animate.js'></script>
    <script src='webjars/angularjs/1.5.5/angular-messages.js'></script>
    <script src='webjars/angular-material-icons/0.7.0/angular-material-icons.js'></script>
    <script src='webjars/angular-material/1.0.7/angular-material.js'></script>
    
    <script src='modules/app/js/app.module.js'></script>
    <script src='modules/contact/js/contact.module.js'></script>
    
    <script src='modules/contact/js/contact.service.js'></script>
    <script src='modules/contact/js/contact.ctrl.js'></script>
    <script src='modules/contact/js/contact-list.ctrl.js'></script>
    <script src='modules/contact/js/contact-detail.ctrl.js'></script>
    <script src='modules/contact/js/contact-form.ctrl.js'></script>
    <script>
        angular.element(document).ready(function(){
            angular.bootstrap(document, ['main']);
        })
    </script>
</head>
<body>
    <div class='layout-column' id='main' ui-view></div>
</body>
</html>