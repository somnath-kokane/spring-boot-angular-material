package com.krestere.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.krestere.entities.ContactEntity;

@RestController
public class ContactsRestController {

	static private List<ContactEntity> list;

	public ContactsRestController() {
		super();
		// TODO Auto-generated constructor stub
		list = new ArrayList<ContactEntity>();
		list.add(new ContactEntity("TerrenceS.Hatfield", "651-603-1723", "TerrenceSHatfield@rhyta.com"));
		list.add(new ContactEntity("ChrisM.Manning", "513-307-5859", "ChrisMManning@dayrep.com"));
	}

	@RequestMapping("/contacts/")
	public List<ContactEntity> getContacts() {

		return list;
	}

	@RequestMapping("/contacts/{id}")
	public ContactEntity getContactDetails(@PathVariable Integer id) {
		ContactEntity contact = list.get(--id);
		return contact;
	}
}
