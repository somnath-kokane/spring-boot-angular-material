package com.krestere.entities;

public class ContactEntity {
	
	private Integer id;
	private String name;
	private String tel;
	private String email;
	
	static private Integer increment = 0;
	
	public ContactEntity(String name, String tel, String email) {
		super();
		++increment;
		this.setId(increment);
		this.setName(name);
		this.setTel(tel);
		this.setEmail(email);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
	
}
